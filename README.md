# Magento Tutorial #

https://devdocs.magento.com/guides/m1x/magefordev/mage-for-dev-2.html


### What is this repository for? ###

* These are my first steps in Magento
* 1.9.4
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Clear your Magento cache and then load any Magento URL with a showConfig=true query string

### What am I looking at? ###

You should be looking at a giant XML file. This describes the state of your Magento system. It lists all modules, models, classes, event listeners or almost anything else you could think of.

For example, consider the config.xml file you created above. If you search the XML file in your browser for the text Configviewer_Model_Observer you'll find your class listed. Every module's config.xml file is parsed by Magento and included in the global config.
